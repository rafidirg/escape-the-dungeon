extends Node

signal stamina_changed(stamina)

export var stamina = 0
export(int) var max_stamina = 200

func _ready():
	stamina = max_stamina
	emit_signal("stamina_changed", stamina)

func sprint():
	stamina -= 5
	stamina = max(0, stamina)
	emit_signal("stamina_changed", stamina)

func regen_stamina():
	stamina += 2
	stamina = min(max_stamina, stamina)
	emit_signal("stamina_changed", stamina)
