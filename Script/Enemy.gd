extends KinematicBody2D

onready var player = get_parent().get_node("Player")

var player_in_range
var player_in_sight
var velocity = Vector2.ZERO
var run_speed = 75
var player_body = null

func _ready():
	pass # Replace with function body.
	
func _physics_process(delta):
	var animation = "Idle"
	SightCheck()
	if player_body:
		velocity = position.direction_to(player.position) * run_speed
		animation = "Walk"
	else:
		velocity = Vector2.ZERO
	velocity = move_and_slide(velocity)
	if $AnimatedSprite.animation != animation:
		$AnimatedSprite.play(animation)
	
func _on_Sight_body_entered(body):
	if body == player:
		player_in_range = true

func _on_Sight_body_exited(body):
	if body == player:
		player_in_range = false

func SightCheck():
	if player_in_range == true:
		var space_state = get_world_2d().direct_space_state
		var sight_check = space_state.intersect_ray(position, player.position, [self], collision_mask)
		if sight_check:
			if sight_check.collider.name == "Player" and player_in_range:
				player_in_sight = true
				player_body = player
			else:
				player_in_sight = false
				player_body = null
	else:
		player_in_sight = false
		player_body = null


func _on_GameOver_body_entered(body):
	if body.get_name() == "Player":
		get_tree().reload_current_scene()
