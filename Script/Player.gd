extends KinematicBody2D

export var MAX_SPEED = 100
export var SPRINT_SPEED = 150
export var ACCELERATION = 400
export var SPRINT_ACCELERATION = 500
export var FRICTION = 400

var motion = Vector2.ZERO
onready var level = get_parent()
onready var stamina = $Stamina

func get_input_axis():
	var animation = "Idle"
	
	var axis = Vector2.ZERO
	axis.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	axis.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	
	if(axis.x != 0 or axis.y !=0):
		animation = "Walk"
		
	$AnimatedSprite.flip_h = (axis.x < 0)
	
	if $AnimatedSprite.animation != animation:
		$AnimatedSprite.play(animation)
	
	return axis.normalized()

func _physics_process(delta):
	var axis = get_input_axis()
	
	if axis == Vector2.ZERO:
		motion = motion.move_toward(Vector2.ZERO, FRICTION * delta)
		stamina.regen_stamina()
		
	elif Input.is_action_pressed("sprint") and stamina.stamina > 0:
		motion += axis * SPRINT_ACCELERATION * delta
		motion = motion.clamped(SPRINT_SPEED * delta)
		stamina.sprint()
		
	else:
		motion += axis * ACCELERATION * delta
		motion = motion.clamped(MAX_SPEED * delta)

	print(stamina)

	move_and_collide(motion)
	
	level.update_fog(get_position()/level.GRID_SIZE)
	
